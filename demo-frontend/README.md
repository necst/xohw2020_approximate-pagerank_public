
1. Install NVM: `wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash; source ~/.bashrc`
2. Install node.js: `nvm install node`
3. Install angulari cli: `npm i g angular/cli`
4. Install all dependencies: `npm install`
6. Change the server endpoint to use the server where your FPGA server is running (in `src/environments/environment.ts`), otherwise leave the default value (which points to a mock backend, and is useful to play with the visualization)
7. Build and start project: `ng serve --prod`
8. Open your browser on http://localhost:4200
