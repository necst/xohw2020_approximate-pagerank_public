export const environment = {
  production: true,
  serviceEndpoints: {
    graph: 'https://xohw-approximate-pagerank.herokuapp.com',
    pagerank: 'https://xohw-approximate-pagerank.herokuapp.com',
  },
};
