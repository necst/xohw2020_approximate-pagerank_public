Team number: xohw20_296

Project name: Accelerating Personalized PageRank on FPGAs for low-latency Recommender Systems

Date: 2020-06-30

Version of uploaded archive: 1.0

University name: Politecnico di Milano

* Supervisor name: Marco Santambrogio
* Supervisor e-mail: marco.santambrogio@polimi.it

Partecipant(s): Alberto Parravicini, Francesco Sgherzi

* Email: alberto.parravicini@polimi.it, francesco1.sgherzi@mail.polimi.it

Board used: Alveo U200, FPGA: xcu200-fsgd2104-2-e

Software Version: Xilinx Vivado HLS 2018.3, Xilinx SDx 2018.3

Brief description of the project:
	Graph-based ranking algorithms finds the most important vertices in a graph data representation and are widely employed in many applications, such as search engines, recommender systems,
	and even Natural Language Processing. In most of these scenarios, low latency and high throughput are deemed more valuable than exact numerical convergence. 
	FPGAs represent a precious instrument to provide quick execution times while offering precise control over the degree of accuracy of the results thanks to reduced-precision fixed-point arithmetic. 
	In this work, we present a new FPGA implementation of the famous Personalized PageRank algorithm, a common building block of recommender systems in e-commerce websites and social networks.
	We propose an optimized streaming implementation of Coordinate Format (COO) sparse matrix multiplication that, along with low-precision fixed-point arithmetic,
	achieves realistic speedups up to 6x over a reference floating-point FPGA architecture and over a state-of-the-art multi-threaded CPU implementation on 8 different data-sets,
	while preserving the numerical fidelity of the results and reaching 42x higher energy efficiency compared to the CPU implementation.
	
	
* Link to project repository: https://bitbucket.org/necst/xohw2020_approximate-pagerank_public/src/master/

Description of archive:

* repository: it contains a copy of the source files used for the project, and the source code of the server used in the demo. The "data" subfolder contains a couple of small graphs that can be used for testing purposes
* demo-frontend: source code of the front-end of the demo
* bitstream: it contains the bitstream required to execute the project
* report: it contains the report for the project

Instructions to build and test project:

* The file `repository/src/fpga/src/csc_fpga/csc_fpga.hpp` contains different useful settings. Default values will be ok to test the project, but you can play with them if desired
	* FIXED_WIDTH: bit-width of fixed-point values, including 1 bit of integer part
	* N_PPR_VERTICES: number of vertices for which PPR is computed in parallel
	* AP_UINT_BITWIDTH: how many COO values (i.e. graph edges) are processed for each clock cycle
	* MAX_VERTICES: maximum number of vertices supported by the graph. The default value (2^14) is small to allow faster build time, and is enough to use the small graphs included in the repository
* To build the project from scratch, and run it on FPGA, perform the following steps
	* `cd repository`
	* `make build TARGET=hw`. This will start the compilation and bitstream generation. The `TARGET_CLOCK` setting in the Makefile allows to specify the desired clock frequency. Whether this value can be achieved depends on the specified bitwidth and other settings, although SDx will try building the project with the highest clock frequency it can.
	* `make host TARGET=hw`. Optional step, it rebuilds the host code. It might be required if the machine where the `build` step was performed has a different CPU architecture than the machine where the compiled host code is executed. If you get errors such as `HAL version 1 not supported` when executing the code, you need to rebuild the host code
	* The file `repository/src/fpga/src/host_demo_cpp` (currently commented out) is used in the demo. It will setup the FPGA and wait indefinitely for data to be written on FIFOs. You can modify the Makefile to compile this file instead, if you want to try the demo by yourself, although the default host file will be enough to test the project and is simpler to build
* The project was tested on an Alveo U200 board with DSA `xilinx_u200_xdma_201830_2`. Remember to `source` the FPGA `setup.sh` on the machine where the code is executed!
* Running the code:
	* `cd repository/build/hw/xilinx_u200_xdma_201830_2`
	* `./approximate_pagerank -d -g <path/to/graph.mtx> -m 100 -t 10 -x <path/to/xclbin>`
	* `-d`: if present, print debug information
	* `-s`: if present, use a small sample graph
	* `-m`: max number of iterations
	* `-e`: error value used as convergence threshold
	* `-a`: value of alpha
	* `-t`: number of times the computation is repeated, for testing
	* `-p`: personalization vertex ID. If not present, use N_PPR_VERTICES random values for testing
	
Link to YouTube Video: https://youtu.be/zPZhwJ0PcVo
