/*
//
// Created by Francesco Sgherzi on 8/10/19.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CL/opencl.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>
#include <math.h>
#include <vector>
#include <getopt.h>
#include <ap_int.h>
#include <chrono>

#include "opencl_utils.hpp"
#include "../../common/csc_matrix/csc_matrix.hpp"
#include "../../common/utils/options.hpp"
#include "../../common/utils/utils.hpp"
#include "../../common/utils/evaluation_utils.hpp"
#include "pagerank_csc.hpp"
#include "pagerank_coo.hpp"
#include "gold_algorithms.hpp"
#include "csc_fpga/csc_fpga.hpp"
#include "coo_fpga/coo_fpga.hpp"

/////////////////////////////
/////////////////////////////

namespace chrono = std::chrono;
using clock_type = chrono::high_resolution_clock;

/////////////////////////////
/////////////////////////////

int main(int argc, char *argv[]) {

	/////////////////////////////
	// Input parameters /////////
	/////////////////////////////

	Options options = Options(argc, argv);
	int debug = options.debug;
	bool use_csc = options.use_csc;
	std::string graph_path = options.graph_path;
//	std::string graph_path = "../../data/graphs/mtx/graph_small_c16.mtx";
	int max_iter = options.max_iter;
	fixed_float alpha = options.alpha;
	fixed_float max_err = options.max_err;
	bool use_sample_graph = options.use_sample_graph;
	uint num_tests = options.num_tests;
	bool undirect_graph = options.undirect_graph;
	std::string xclbin_path = options.xclbin_path;

	std::string golden_res = "../../data/graphs/other/test/results.txt";

	/////////////////////////////
	// Setup ////////////////////
	/////////////////////////////

	csv_results_t results;
	results.fixed_float_width = FIXED_WIDTH;
	results.fixed_float_scale = FIXED_INTEGER_PART;

	std::vector<std::string> target_devices = { "xilinx_u200_xdma_201830_1" };
	// When running software emulation the program is launched from Emulation-SW/approximate_pagerank-Default;
	std::vector<std::string> kernels = { xclbin_path };
	std::string kernel_name = "multi_ppr_main";

	ConfigOpenCL config(kernel_name);
	setup_opencl(config, target_devices, kernels, debug);

	/////////////////////////////
	// Setup ////////////////////
	/////////////////////////////

    // FIFOs in the /tmp/ folder;
    const char *myfifo_in = "/tmp/myfifo_in";
    const char *myfifo_out = "/tmp/myfifo_out";

    // Creating the FIFOs;
    printf("Receiver - Creating FIFOs...\n");
    mkfifo(myfifo_in, 0666);
    mkfifo(myfifo_out, 0666);

    // Opening connections to the FIFOs;
    printf("Receiver - Opening FIFOs...\n");
    int fifo_in = open(myfifo_in, O_RDWR);
    int fifo_out = open(myfifo_out, O_RDWR);

	/////////////////////////////
	// Load data ////////////////
	/////////////////////////////

	// Use a sample graph if required;
	if (use_sample_graph) {
		if (debug) {
			std::cout << "Using sample graph located at: " << DEFAULT_MTX_FILE
					<< std::endl;
		}
		use_csc = false;
		graph_path = DEFAULT_MTX_FILE;
	}

	// Load dataset and create auxiliary vectors;
	auto start_1 = clock_type::now();
	csc_t input_m;
	if (!use_csc) {
		input_m = load_graph_mtx(graph_path, debug, undirect_graph);
	} else {
		input_m = load_graph_csc(graph_path, debug);
	}
	auto end_1 = clock_type::now();
	auto start_2 = clock_type::now();
	csc_fixed_fpga_t f_input_m = convert_to_fixed_point_fpga(input_m);
	auto end_2 = clock_type::now();
	// Convert CSC to COO;
	auto start_3 = clock_type::now();
	coo_fixed_fpga_t coo(f_input_m, true);
	auto end_3 = clock_type::now();

	auto loading_time = chrono::duration_cast<chrono::milliseconds>(end_1 - start_1).count();
	auto to_fixed_time = chrono::duration_cast<chrono::milliseconds>(end_2 - start_2).count();
	auto to_coo = chrono::duration_cast<chrono::milliseconds>(end_3 - start_3).count();

	results.n_edges = f_input_m.col_val.size();
	results.n_vertices = f_input_m.col_ptr.size() - 1;
	std::vector<index_type, aligned_allocator<index_type>> personalization_vertices(N_PPR_VERTICES, 0);
	for (int i = 0; i < N_PPR_VERTICES; ++i) {
		personalization_vertices[i] = rand() % results.n_vertices;
	}

	if (debug) {
		std::cout << "\n----------------\n- Graph Summary -\n----------------"
				<< std::endl;
		std::cout << "- |V| = " << results.n_vertices << std::endl;
		std::cout << "- |E| = " << results.n_edges << std::endl;
		std::cout << "- Undirected: " << (undirect_graph ? "True" : "False")
				<< std::endl;
		std::cout << "- Alpha: " << alpha << std::endl;
		std::cout << "- Max. error: " << max_err << std::endl;
		std::cout << "- Max. iterations: " << max_iter << std::endl;
		std::cout << "- Personalization vertices: "
				<< format_array(personalization_vertices.data(), N_PPR_VERTICES)
				<< std::endl;
		std::cout << "----------------" << std::endl;
		std::cout << "- Loading time: " << loading_time / 1000 << " sec"
				<< std::endl;
		std::cout << "- Float-to-fixed time: " << to_fixed_time / 1000 << " sec"
				<< std::endl;
		std::cout << "- CSC-to-COO: " << to_coo / 1000 << " sec" << std::endl;
		std::cout << "- Tot. time: "
				<< (loading_time + to_fixed_time + to_coo) / 1000 << " sec"
				<< std::endl;
		if (!use_csc) {
			long filesize_mb = get_file_size(graph_path) / 1000000;
			double throughput =
					loading_time > 0 ? 1000 * filesize_mb / loading_time : 0;
			std::cout << "- File size: " << filesize_mb << " MB" << std::endl;
			std::cout << "- Throughput: " << throughput << " MB/sec"
					<< std::endl;
		}
		std::cout << "----------------" << std::endl;
	}

	// Setup the PageRank data structure;
	PageRankCOO pr = PageRankCOO(results.n_vertices, results.n_edges, &coo,
			max_iter, alpha, max_err, personalization_vertices);

	/////////////////////////////
	// Setup kernel /////////////
	/////////////////////////////

	// Create Kernel Arguments
	pr.preprocess_inputs();
	pr.setup_inputs(config, debug);
	results.transfer_time = pr.transfer_input_data(config, true, debug);

	/////////////////////////////
	// Execute the kernel ///////
	/////////////////////////////

	std::vector<double> trans_times;
	std::vector<double> exec_times;

	int i = 0;
	while(true) {

		std::cout << "Waiting for data to be written on FIFO..." << std::endl;

		// Read from the FIFO;
		ssize_t res_trans;
		// Read the number of vertices;
		int num_ppr_vertices_todo = 0;
		res_trans = read(fifo_in, &num_ppr_vertices_todo, sizeof(int));
		// Read the top values to obtain;
		int max_top_k_values = 0;
		res_trans = read(fifo_in, &max_top_k_values, sizeof(int));

		// Read the vertices to process;
		std::vector<int> ppr_vertices_todo(num_ppr_vertices_todo, 0);
		res_trans = read(fifo_in, ppr_vertices_todo.data(), ppr_vertices_todo.size() * sizeof(int));
		std::vector<index_type> ppr_vertices_new;
		ppr_vertices_new.insert(ppr_vertices_new.end(), ppr_vertices_todo.begin(), ppr_vertices_todo.end());

		std::cout << "Num PPR vertices: " << num_ppr_vertices_todo << "; Top-K vertices: " << max_top_k_values << std::endl;

		// Reset the result;
		results.transfer_time = pr.reset(config, ppr_vertices_new, true, debug);

		trans_times.push_back(results.transfer_time);
		results.execution_time = pr.execute(config, true, debug);

		exec_times.push_back(results.execution_time);

		auto results_fixed_idx = std::vector<std::vector<int>>();
		auto results_fixed_ppr = std::vector<std::vector<float>>();
		for (int j = 0; j < std::min(N_PPR_VERTICES, num_ppr_vertices_todo); ++j) {
			int cur_begin = j * pr.N;
			int cur_end = (j + 1) * pr.N;
			auto tmp = std::vector<fixed_float>(pr.result.data() + cur_begin,
					pr.result.data() + cur_end);
			auto tmp_sorted = sort_pr_with_value(pr.N, tmp.data());

			std::vector<int> results_fixed_idx_vec(std::min(max_top_k_values, (int) pr.N));
			std::vector<float> results_fixed_ppr_vec(std::min(max_top_k_values, (int) pr.N));
			for (int q = 0; q < std::min(max_top_k_values, (int) pr.N); q++) {
				results_fixed_idx_vec[q] = (int) tmp_sorted[q].first;
				results_fixed_ppr_vec[q] = tmp_sorted[q].second.to_float();

				if (debug) {
					std::cout << q << "]: " << results_fixed_idx_vec[q] << ", value=" << results_fixed_ppr_vec[q] << std::endl;
				}
			}

			// Write to FIFO;
			res_trans = write(fifo_out, results_fixed_idx_vec.data(), results_fixed_idx_vec.size() * sizeof(int));
			res_trans = write(fifo_out, results_fixed_ppr_vec.data(), results_fixed_ppr_vec.size() * sizeof(float));

			results_fixed_idx.push_back(results_fixed_idx_vec);
			results_fixed_ppr.push_back(results_fixed_ppr_vec);
		}

		if (!debug) {
			if (i== 0) {
				std::cout << "fixed_float_width,fixed_float_scale,v,e,"
						<< "execution_time,transfer_time"
						<< std::endl;
			}
			std::cout << results.fixed_float_width << ","
					<< results.fixed_float_scale << "," << results.n_vertices
					<< "," << results.n_edges << "," << results.execution_time
					<< "," << results.transfer_time
					<< std::endl;
			i++;
		}
	}
	if (debug) {
		int old_precision = cout.precision();
		cout.precision(2);
		std::cout << "\n----------------\n- Results ------\n----------------"
				<< std::endl;
		std::cout << "Mean transfer time:  " << mean(trans_times) << "Â±"
				<< st_dev(trans_times) << " ms;" << std::endl;
		std::cout << "Mean execution time: " << mean(exec_times) << "Â±"
				<< st_dev(exec_times) << " ms;" << std::endl;
		std::cout << "----------------" << std::endl;
		cout.precision(old_precision);
	}
}

*/
