const fs = require("fs")
const { spawn } = require('child_process');

const asyncCreateFifo = async (fifoPath, mode) => {
    const process = spawn("mkfifo", [fifoPath])
    return new Promise((resolve, reject) => {
        process.on("exit", (status) => {
            const fd = fs.openSync(fifoPath, mode)

            let createStreamFn
            if(mode.includes("r")) createStreamFn = fs.createReadStream
            else createStreamFn = fs.createWriteStream
            
            resolve(createStreamFn(null, {fd}))
        })
    })
}

const initFifo = async (fifoPath, mode) => {
    
    // If the fifo exists, delete it and recreate it
    if(fs.existsSync(fifoPath)) fs.unlinkSync(fifoPath)

    const curStream = await asyncCreateFifo(fifoPath, mode)
    return curStream
}

module.exports = {
    initFifo
}