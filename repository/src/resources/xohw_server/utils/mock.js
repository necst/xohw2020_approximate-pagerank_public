const respondWithMockResponse = (req, res) => {
    const {
        vertices: requestedVertices
    } = req.vertexSource

    const request = requestedVertices
    const response = requestedVertices.map(vertex => {
        let ranking = new Set()
        let value   = []
        for(let i = 0; i < 100; i++){
            ranking.add(Math.floor(Math.random() * 100))
        }

        for(let i = 0; i < ranking.size; i++){
            value.push(1 - i / ranking.size)
        }

        ranking = Array.from(ranking)

        const jointResults = value.map((el, idx, arr) => {
            return {
                vertex: ranking[idx], 
                value: el
            }
        })

        return {
            id: vertex, 
            results: jointResults,
        }

    })

    res.status(200).send({
        request, 
        response
    })

}

module.exports = {
    respondWithMockResponse
}