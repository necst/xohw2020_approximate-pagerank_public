

// LOGIC: 
// If the number of vertices is either 0 or req.vertices is undefined respond with HTTP 400
// If the vertex number is greater than `PPRMaxVertexNumber` respond with HTTP 413
// If any chosen vertex is greater than `vertexCount` respond with HTTP 400
// Otherwise forward the request to the next middleware
const validateVertexNumberMiddleware = (PPRMaxVertexNumber, vertexCount) => {
    return (req, res, next) => {

        const {
            vertices
        } = req.vertexSource

        if(!vertices || !Array.isArray(vertices)) {
            res.status(400).send({
                message: `Missing vertices array.`
            })
            return
        }
        if(vertices.length > PPRMaxVertexNumber) {
            res.status(413).send({
                message: `Can process at most ${PPRMaxVertexNumber} vertices, got ${vertices.length}.`
            })
            return
        }
        
        vertices.forEach((element, index) => {
            if(element > vertexCount) {
                res.status(422).send({
                    message: `Element at index ${index} is beyond graph size. Got ${element}, expected < ${vertexCount}`
                })
                return
            }
        });
        
        console.log("Validated vertices: ")
        console.log(vertices)
        
        console.log("Parsing vertices as Int and removing duplicates")
        req.query.vertices = Array.from(new Set(req.query.vertices.map(vertex => parseInt(vertex))))
        next()
    }
}

// Allow from given origins
const corsMiddleware = (allowedOrigin) => {
    return (req, res, next) => {
        res.header('Access-Control-Allow-Origin', allowedOrigin)
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        res.type('application/json')
        next()
    }
}

const allowRequestMethodsMiddleware = (methods) => {
    return (req, res, next) => {
        if(methods.includes(req.method)) next()
        else {
            res.status(405).send({
                message: `Method ${req.method} not allowed.`
            })
            return
        }
    }
}

const generateSourceMiddleware = (req, res, next) => {
    if(req.method === "GET") req.vertexSource = req.query
    else if(req.method === "POST") req.vertexSource = req.body
    next()
}

module.exports = {
    validateVertexNumberMiddleware, 
    allowRequestMethodsMiddleware,
    generateSourceMiddleware, 
    corsMiddleware
}