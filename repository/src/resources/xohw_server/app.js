const express = require("express")
const bodyParser = require("body-parser")

const {
    DEFAULT_MAX_VERTICES,
    DEFAULT_GRAPH_VERTICES_SIZE,
    DEFAULT_PORT, 
    DEFAULT_FIFO_READ_PATH, 
    DEFAULT_FIFO_WRITE_PATH, 
    DEFAULT_VERTEX_SEPARATOR, 
    DEFAULT_DISABLE_DEBUG
} = require("./utils/config")

const {
    initFifo
} = require("./utils/fifo")

const {
    respondWithMockResponse
} = require("./utils/mock")

const {
    validateVertexNumberMiddleware: validateVertexNumber, 
    allowRequestMethodsMiddleware: allowRequestMethods,
    generateSourceMiddleware: generateSource,
    corsMiddleware: cors
} = require("./utils/middlewares")


const PORT                = parseInt(process.env["PORT"]) || DEFAULT_PORT
const MAX_VERTICES        = parseInt(process.env["MAX_VERTICES"]) || DEFAULT_MAX_VERTICES
const GRAPH_VERTICES_SIZE = parseInt(process.env["GRAPH_VERTICES_SIZE"]) || DEFAULT_GRAPH_VERTICES_SIZE
const FIFO_READ_PATH      = process.env["FIFO_READ_PATH"] || DEFAULT_FIFO_READ_PATH
const FIFO_WRITE_PATH     = process.env["FIFO_WRITE_PATH"] || DEFAULT_FIFO_WRITE_PATH
const MOCK                = process.env["MOCK"]
const VERTEX_SEPARATOR    = process.env["VERTEX_SEPARATOR"] || DEFAULT_VERTEX_SEPARATOR
const DISABLE_DEBUG       = process.env["DISABLE_DEBUG"] || DEFAULT_DISABLE_DEBUG

if(DISABLE_DEBUG) {
    // If in """production""" disable logging altogheter
    console.log = () => {}
}

let readStream
let writeStream
// Because top level await only works in ES modules
(async () => {
    readStream = await initFifo(FIFO_READ_PATH, "r+")
    writeStream = await initFifo(FIFO_WRITE_PATH, "w+")
})();

const app = express()

// To properly parse JSON requests
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors("*"))

app.all(
    "/compute", 
    allowRequestMethods(["GET", "POST"]),
    generateSource,
    validateVertexNumber(MAX_VERTICES, GRAPH_VERTICES_SIZE), 
    async (req, res, next) => {

        if (MOCK){
            respondWithMockResponse(req, res)
            return 
        }

        const {
            vertices 
        } = req.vertexSource

        writeStream.write(vertices.join())

        let buffers      = []
        let curVertexIdx = 0
        let tmpVertex    = {}

        readStream.on("data", (bytes) => {
            console.log(`Number of Elements yelded: ${buffers.length + 1}`)
            // Each vertex has also its ppr value
            if(buffers.length < vertices.length) {
                if(!tmpVertex.id){
                    console.log(`Ranking for vertex ${vertices[curVertexIdx]}`)
                    tmpVertex.id = vertices[curVertexIdx]
                    tmpVertex.ranking = String.fromCharCode(...bytes).trim().split(VERTEX_SEPARATOR).map(x => parseInt(x))
                } else {
                    console.log(`Value for vertex ${vertices[curVertexIdx]}`)
                    tmpVertex.rankingValue =  String.fromCharCode(...bytes).trim().split(VERTEX_SEPARATOR).map(x => parseFloat(x))

                    // Here I've parsed both values and indices
                    const jointResults = tmpVertex.rankingValue.map((el, idx) => {
                        return {
                            vertex: tmpVertex.ranking[idx], 
                            value: el
                        }
                    })
                    tmpVertex.results = jointResults
                    // Undefined elements are cancelled during serialization (eg stringfy) which is much faster than doing `delete tmpVertex.ranking`
                    tmpVertex.ranking = undefined
                    tmpVertex.rankingValue = undefined
                    buffers.push(tmpVertex)
                    tmpVertex = {}
                    curVertexIdx ++
                }
   
            }

            

            if(buffers.length == vertices.length) {
                res.status(200).send({
                    request: vertices, 
                    response: buffers
                })
            }
        })
})

app.listen(PORT, () => {

    if(MOCK) 
        console.log("Mocking enabled")

    console.log(`Server listening on port ${PORT}`)
})