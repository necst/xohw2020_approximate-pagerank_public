#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 6 15:20:30 2020

@author: aparravi
"""

import os
import random
import struct

# For testing purposes only, use the FPGA host instead!

if __name__ == "__main__":

	# FIFO names, use exactly these values!
	in_fifo_path = "/tmp/myfifo_in"
	out_fifo_path = "/tmp/myfifo_out"

	# Create FIFO
	if os.path.exists(in_fifo_path):
		os.remove(in_fifo_path)
		os.mkfifo(in_fifo_path)
	if os.path.exists(out_fifo_path):
		os.remove(out_fifo_path)
		os.mkfifo(out_fifo_path)

	n_vertices = 3
	n_top_k = 10

	num_tests = 7

	# Max vertex, we use some placeholder value (don't send values >= this value!)
	max_vertex = 100

	with open(in_fifo_path, "rb") as fifo_in:
		with open(out_fifo_path, "wb") as fifo_out:
			for i in range(num_tests):

			# Read values 1 int at a time;
			n_vertices_temp = int.from_bytes(fifo_in.read(4), "big")
			n_top_k_temp = int.from_bytes(fifo_in.read(4), "big")

			vertices_temp = []

			for j in range(n_vertices):
				vertices_temp += [int.from_bytes(fifo_in.read(4), "big")]

			print(n_vertices_temp)
			print(n_top_k_temp)
			print(vertices_temp)

			# Write back some random integers and floats;
			print("Write to FIFO")
			for j in range(n_top_k_temp):
				fifo_out.write(random.randint(0, max_vertex - 1).to_bytes(4, "big"))
			for j in range(n_top_k_temp):
				fifo_out.write(bytearray(struct.pack("f", random.random())))
			fifo_out.flush()

			os.remove(in_fifo_path)
			os.remove(out_fifo_path)
