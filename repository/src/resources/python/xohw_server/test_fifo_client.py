#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 6 14:37:20 2020

@author: aparravi
"""

import os
import random
import struct


if __name__ == "__main__":

	# FIFO names, use exactly these values!
	in_fifo_path = "/tmp/myfifo_in"
	out_fifo_path = "/tmp/myfifo_out"

	n_vertices = 3
	n_top_k = 10

	num_tests = 7

	# Max vertex, we use some placeholder value (don't send values >= this value!)
	max_vertex = 100

	# Open it just once, not inside the loop!
	with open(in_fifo_path, "wb") as fifo_in:
		with open(out_fifo_path, "rb") as fifo_out:
			for i in range(num_tests):

				print("write to FIFO")

				# Write number of PPR vertices (1 <= x <= 8)
				fifo_in.write(n_vertices.to_bytes(4, "little"))
				fifo_in.write(n_top_k.to_bytes(4, "little"))

				# Write random vertices;
				for j in range(n_vertices):
					r = random.randint(0, max_vertex - 1)
					print(r)
					fifo_in.write(r.to_bytes(4, "little"))
				fifo_in.flush()

				# Read results;
				for j in range(n_vertices):
					top_vertices = []
					for q in range(n_top_k):
						top_vertices += [int.from_bytes(fifo_out.read(4), "little")]
					top_vertices_ppr = []
					for q in range(n_top_k):
						top_vertices_ppr += [struct.unpack('f', fifo_out.read(4))[0]]
					print(top_vertices, top_vertices_ppr)
